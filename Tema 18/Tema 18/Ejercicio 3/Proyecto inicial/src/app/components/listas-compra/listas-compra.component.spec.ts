import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListasCompraComponent } from './listas-compra.component';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { MatListModule } from '@angular/material/list';

describe('ListasCompraComponent', () => {
  let component: ListasCompraComponent;
  let fixture: ComponentFixture<ListasCompraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListasCompraComponent ],
      imports: [
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        // provideAuth(() => getAuth()),
        provideFirestore(() => getFirestore()),
        MatListModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListasCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

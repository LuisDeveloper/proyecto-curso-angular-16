import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditarProductoComponent } from 'src/app/dialogs/editar-producto/editar-producto.component';
import { EliminarProductoComponent } from 'src/app/dialogs/eliminar-producto/eliminar-producto.component';
import { Producto } from 'src/app/models/producto';
import { ListasCompraService } from 'src/app/services/listas-compra.service';
import { MatListOption } from '@angular/material/list';

@Component({
  selector: 'app-item-producto',
  templateUrl: './item-producto.component.html',
  styleUrls: ['./item-producto.component.scss']
})
export class ItemProductoComponent {
  @Input() datosProducto: Producto = {
    id: '',
    nombre: '',
    unidades: 0,
    marcado: false
  };
  @Input() idListaCompra: string = '';

  constructor(
    private listasCompraService: ListasCompraService,
    private dialog: MatDialog
  ) {}

  abrirEditarProducto() {
    const dialog = this.dialog.open(EditarProductoComponent, {
      data: {
        nombre: this.datosProducto.nombre,
        unidades: this.datosProducto.unidades
      }
    });

    dialog.afterClosed().subscribe({
      next: async (result: Producto) => {
        if (result) {
          await this.listasCompraService.updateProductoFromListaCompra(this.idListaCompra, this.datosProducto.id as string, result);
        }
      },
      error: (error) => console.log(error)
    })
  }

  abrirEliminarProducto() {
    const dialog = this.dialog.open(EliminarProductoComponent);

    dialog.afterClosed().subscribe({
      next: async (respuesta: boolean) => {
        if (respuesta) {
          await this.listasCompraService.deleteProductoFromListaCompra(
            this.idListaCompra, this.datosProducto.id as string
          );
        }
      },
      error: (error) => console.log(error),
    });
  }

  onClick(evt: Event) {
    evt.stopPropagation();
  }
  async toggleSelection(listOption: MatListOption) {
    await this.listasCompraService.updateProductoFromListaCompra(
      this.idListaCompra, this.datosProducto.id as string, {
        marcado: listOption.selected
      });
  }
}

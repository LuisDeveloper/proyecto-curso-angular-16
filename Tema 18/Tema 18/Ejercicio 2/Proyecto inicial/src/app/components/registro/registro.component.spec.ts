import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroComponent } from './registro.component';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';

describe('RegistroComponent', () => {
  let component: RegistroComponent;
  let fixture: ComponentFixture<RegistroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistroComponent ],
      imports: [
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        provideAuth(() => getAuth()),
        MatSnackBarModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatInputModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe mostrar error de email requerido', () => {
    component.registroForm.controls['email'].setValue('');
    const submitButton = fixture.nativeElement.querySelector('button[type=submit]');
    submitButton.click();
    expect(component.registroForm.controls['email'].getError('required')).toBeTrue();
  });

  it('Debe mostrar error por contraseñas distintas', () => {
    component.registroForm.controls['password'].setValue('123456');
    component.registroForm.controls['repitePassword'].setValue('1234567');
    const submitButton = fixture.nativeElement.querySelector('button[type=submit]');
    submitButton.click();
    expect(component.registroForm.controls['repitePassword'].getError('passwordsIguales')).toBeTrue();
  });
});

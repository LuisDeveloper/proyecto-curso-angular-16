import { Component } from '@angular/core';
import { Form } from '@angular/forms';
import { DummyJsonService } from './dummy-json.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tema10 Ejercicio02';

  quote: any = '';
  todo?: Form;

  constructor(private servicio: DummyJsonService) {
    this.getRandomQuote();    
  }

  getRandomQuote() {
    console.log('clicked');
    this.servicio.getQuote().subscribe({
      next: (response: any) => {
        this.quote = {
          text: response.quote,
          author: response.author
        };
        console.log(this.quote);
        console.log(this.quote.text);
      },
      error: (err) => console.log(err)
    });
  }
  addQuote() {
    this.servicio.postQuote(this.todo);
  }
}

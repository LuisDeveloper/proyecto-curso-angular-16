import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, map, shareReplay } from 'rxjs';
import { ListasCompraService } from './services/listas-compra.service';
import { NuevaListaComponent } from './dialogs/nueva-lista/nueva-lista.component';
import { ListaCompra } from './models/lista-compra';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  mostrarFormularios: boolean = true;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dialog: MatDialog,
    private listasCompraService: ListasCompraService,
    private route: ActivatedRoute,
    private router: Router, 
    private authenticationService: AuthenticationService
  ) { }


  ngOnInit(): void {
    this.router.events.forEach((e: any) => {
      if (e instanceof NavigationEnd) {
        if (
          this.route.root.firstChild?.snapshot.routeConfig?.path === 'login' ||
          this.route.root.firstChild?.snapshot.routeConfig?.path === 'registro') {
            this.mostrarFormularios = true;
        } else {
          this.mostrarFormularios = false;
        }
      } 
    });
  }

  async doLogout() {
    try {
      await this.authenticationService.logout();
      this.authenticationService.isAuthenticated = false;
      this.router.navigate(['/login']);
    } catch(exception) {
      console.log(exception);
    }
  }

  abrirDialogoNuevaLista() {
    const dialog = this.dialog.open(NuevaListaComponent);

    dialog.afterClosed().subscribe({
      next: async (nombreLista: string) => {
        if (nombreLista && nombreLista.trim() !== '') {
          await this.listasCompraService.createListaCompra({
            nombre: nombreLista,
            productos: []
          } as ListaCompra);
        }
      },
      error: (error) => console.log(error)
    });
  }
}

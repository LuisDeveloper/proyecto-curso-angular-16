Para que haya menos archivos, se ha eliminado la carpeta node_modules. Para regenerarla y que el proyecto
funcione, hay que ejecutar en consola `npm install`. Tras la instalación de dependencias, ya
se podrá ejecutar correctamente el proyecto.
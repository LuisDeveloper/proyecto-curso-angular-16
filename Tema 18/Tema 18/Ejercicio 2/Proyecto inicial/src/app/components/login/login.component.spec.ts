import { ComponentFixture, TestBed, async, tick } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, RouterModule } from '@angular/router';
import { ProductosComponent } from '../productos/productos.component';
import {Location} from '@angular/common';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;
  let location: Location;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        provideAuth(() => getAuth()),
        MatSnackBarModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        RouterModule.forRoot([]),
        RouterTestingModule.withRoutes([
          { path: '', component: ProductosComponent, pathMatch: 'full' },
          { path: 'login', component: LoginComponent }
        ]),
        MatInputModule,
        BrowserAnimationsModule
        // provideFirestore(() => getFirestore()),
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

/*   it('Debe hacer login y llevarnos a la pantalla principal', () => {
    router.navigate(['/login']).then(() => {
      const controles = component.loginForm.controls;
      controles['email'].setValue('test@test.com');
      controles['password'].setValue('123456');
      const submitButton = fixture.nativeElement.querySelector('button[type=submit]');
      submitButton.click();
      fixture.detectChanges();
      const snackBar = fixture.nativeElement.querySelector('.mat-mdc-snack-bar-label');
    })
  }); */
});

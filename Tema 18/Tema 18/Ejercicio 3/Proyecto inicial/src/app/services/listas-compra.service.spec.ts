import { TestBed } from '@angular/core/testing';

import { ListasCompraService } from './listas-compra.service';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { environment } from 'src/environments/environment';

describe('ListasCompraService', () => {
  let service: ListasCompraService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        // provideAuth(() => getAuth()),
        provideFirestore(() => getFirestore()),
      ]
    });
    service = TestBed.inject(ListasCompraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

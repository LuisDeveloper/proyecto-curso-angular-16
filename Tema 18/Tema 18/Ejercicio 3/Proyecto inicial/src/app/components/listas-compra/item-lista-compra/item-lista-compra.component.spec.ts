import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemListaCompraComponent } from './item-lista-compra.component';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';
import { EditarListaComponent } from 'src/app/dialogs/editar-lista/editar-lista.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ListaCompra } from 'src/app/models/lista-compra';
import { NO_ERRORS_SCHEMA } from '@angular/compiler';

describe('ItemListaCompraComponent', () => {
  let component: ItemListaCompraComponent;
  let fixture: ComponentFixture<ItemListaCompraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemListaCompraComponent, EditarListaComponent ],
      imports: [
        FormsModule,
        MatDialogModule,
        MatListModule,
        MatIconModule,
        MatMenuModule,
        RouterTestingModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        BrowserAnimationsModule,
        provideFirebaseApp(() => initializeApp(environment.firebase)),
        // provideAuth(() => getAuth()),
        provideFirestore(() => getFirestore()),
      ],
      providers: [
        {provide: MatDialogRef, useValue: {}},
        {provide: MAT_DIALOG_DATA, useValue: []},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemListaCompraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /* it('Debe cambiar el nombre de la lista de la compra', () => {
    const nuevoNombre = Math.random().toString(36).substring(2,7);
    component.listaCompra = {
      id: 'lista1',
      nombre: 'Lista 1',
      productos: []
    } as ListaCompra;

    fixture.detectChanges(); 
    
    const dialog = component['dialog'].open(EditarListaComponent, { data: component.listaCompra.nombre});
    
    console.log(fixture.nativeElement.querySelector('button[color=primary]'));


    expect(component.listaCompra.nombre).toBe(nuevoNombre);
  }); */
});

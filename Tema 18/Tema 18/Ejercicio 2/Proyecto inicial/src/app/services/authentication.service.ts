import { Injectable } from '@angular/core';
import { Auth, UserCredential, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
/**
 * Servicio que se encarga del registro y la autenticación del suaurio en la aplicación a través de Firebase
 * con correo electrónico y contraseña.
 */
export class AuthenticationService {

  isAuthenticated: boolean = false;

  constructor(private authFirebase: Auth) { }

  /**
   * Método que permite registrar un nuevo usuario en la aplicación a partir del correo electrónico y una contraseña.
   * @param {string} email Correo electrónico del usuario
   * @param {string} password Contraseña del usuario
   * @returns {Promise<UserCredential>} Promesa con las creenciales del usuario que se ha creado
   */
  register(email: string, password: string): Promise<UserCredential> {
    return createUserWithEmailAndPassword(this.authFirebase, email, password);
  }

  /**
   * Método para la identificación del usuario en la aplicación a partir del correo electrónico y una contraseña.
   * @param {string} email Correo electrónico del usuario
   * @param {string} password Contraseña del usuario
   * @returns {Promise<UserCredential>} Promsea con las credenciales del usuario que se ha identificado
   */
  login(email: string, password: string): Promise<UserCredential> {
    return signInWithEmailAndPassword(this.authFirebase, email, password);
  }
  /**
* Método para cerrar la sesión del usuario en la aplicación
* @returns {Promise<void>} Promesa que indica que se ha producido el cierre de sesión
*/
  logout(): Promise<void> {
    return signOut(this.authFirebase);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DummyJsonService {
  private urlApi: string = 'https://dummyjson.com/';
  constructor(private http: HttpClient) { }


  getQuote(): Observable<Object> {
    return this.http.get(this.urlApi + 'quotes/random');
  }
  postQuote(quote: object) {
    return this.http.post(this.urlApi + 'quotes/add', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        quote
      })
    });
  }
}
